<?php
/**
 * Created by Bigmidia
 * User: Eduardo Estevão
 * Date: 15/07/2016
 * Time: 12:32
 */


namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CloudFlareApi extends Model
{
    private $auth_email="";
    private $auth_key="";
    private $content_type="application/json";
    private $url="https://api.cloudflare.com/client/v4/user/";

    public function blockIP($ip){
        $this->url=$this->url."firewall/access_rules/rules";

        $config['target']="ip";
        $config['value']=$ip;

        $vetor['mode']="block";
        $vetor['configuration']=$config;
        $vetor['notes']="Blocked by nginx logs";
       $json=$this->exec($vetor,"POST");

        $dados=json_decode($json);

        return $dados->result->id;

    }
    public function unBlockIP($id){
        $this->url=$this->url."firewall/access_rules/rules/".$id;
        $json=$this->exec("","DELETE");
    }
    private function exec($data,$method){
        $ch = curl_init();
        $headers = array(
            'X-Auth-Email: '.$this->auth_email,
            'X-Auth-Key: '.$this->auth_key,
            'Content-Type: '.$this->content_type,
        );

        if($data!=""){
            $json = json_encode($data);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        }

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $dados=curl_exec($ch);
        curl_close($ch);

        return $dados;

    }

}
