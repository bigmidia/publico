<?php
/**
 * Created by Bigmidia
 * User: Eduardo Estevão
 * Date: 15/07/2016
 * Time: 11:46
 * base: https://www.sans.org/reading-room/whitepapers/logging/detecting-attacks-web-applications-log-files-2074
 * base: https://www.sans.org/reading-room/whitepapers/detection/identify-malicious-http-requests-34067
 * base: https://www.sans.org/reading-room/whitepapers/logging/evil-lens-web-logs-33950
 *
 */

namespace app\commands;


use app\models\CloudFlareApi;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Eduardo Estevão <eduardo@Bigmidia.com>
 * @author Daniel Alves <daniel@Bigmidia.com>
 *
 */
class LogAnalyzerController extends Controller
{

    private $max_size=999526601;//bytes
    private $last_time="";
    private $diferenca=120;//segundos
    private $tempoBloqueio=28800;//segundos
    private $suspeitos=array();
    private $max_retry=4;//numero de tentativas antes de bloquear o IP
    private $runtimeLog="";
    private $restricaoLog="";
    private $permanenteLog="";
    private $gotchas=array('proc/self','../../','select(','union all','information_schema','','character_set','concat','passwd','access_log','error_log','r57.php','c99.php','sqlmap','acunetix','netsparker','<?php');
    private $ER=array('/((\%3C)|<)[a-z0-9\%]*((\%3E)|>)/ix',
                    "/\w*((\%27)|(\'))(\s|\+|\%20)*((\%6F)|o|(\%4F))((\%72)|r|(\%52))/i",
                    "/((\%27)|(\') |(\;))(select|union|insert|update|delete|replace|truncate)/ix");
    /**
     * Analisa o log fornecido
     * @param string $log caminho do log
     */
    public function actionIndex($log = '')
    {

        if($log==""){
            echo "Arquivo log nao especificado, informe um arquivo de log para ser aberto\n\r";
            die();
        }
        //LIMPA CACHE DE ARQUIVOS
        clearstatcache();

		//verifica se o arquivo existe
        if(!file_exists($log)){
            echo "O arquivo informado nao existe.\n\r";
            die();
        }
		//evita analise de arquivos grandes
        if(filesize($log) > $this->max_size){
            echo "O arquivo informado tem um tamanho maior que o permitido. \n\r";
            die();
        }
		
		//log em que fica a requisição bloqueada
        $this->runtimeLog=\Yii::getAlias('@runtime').DIRECTORY_SEPARATOR."heimdall.log";
		//log "jail" onde fica os ips banidos e o tempo até serem liberados 
        $this->restricaoLog=\Yii::getAlias('@runtime').DIRECTORY_SEPARATOR."heimdall_block.log";
		//log permamente para analisar o historico do script
        $this->permanenteLog=\Yii::getAlias('@runtime').DIRECTORY_SEPARATOR."heimdall_history.log";
        //abre o arquivo para leitura
        $handle = fopen($log, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $dados=explode('|-|',$line);
                //verifica a existência da data
                if(!isset($dados[2])){
                    continue;
                }

                //verifica se o evento ocorreu nos tempo definido
                if(((int)$dados[2])< (time()-$this->diferenca)){
                    continue;
                }
				//confere se contem alguma string nao permitida
                if($this->strpos_array($line,$this->gotchas)!==FALSE){

                    if(isset($dados[0]) ){
                        $this->somaPontos($dados[0]);
                        $this->logBloqueio($line);
                        echo $line;
                    }
                }
				//confere se contem expressoes regulares nao permitidas
                foreach($this->ER as $regex){
                    if (preg_match($regex, $line)) {
                        if(isset($dados[0]) ){
                            $this->somaPontos($dados[0]);
                            $this->logBloqueio($line);
                            echo $line;
                        }
                    }
                }
            }
            fclose($handle);
        } else {
            // error opening the file.
        }

        if(count($this->suspeitos)>0){
            foreach($this->suspeitos as $ip=>$quantidade){
                if($quantidade>$this->max_retry){
                    echo $ip." - ".$quantidade."\r\n";
                    $cloud=new CloudFlareApi();
                    $id=$cloud->blockIP($ip);

                    $this->blackListIp($ip,$id,(time()+$this->tempoBloqueio));
                }
            }
        }

    }
    private function somaPontos($ip){

        $ip=trim($ip);
        if(! isset($this->suspeitos[$ip]) ){
            $this->suspeitos[$ip]=1;
        }else{
            $this->suspeitos[$ip]+=1;
        }
    }
    private function logBloqueio($linha){
       try{
            file_put_contents($this->runtimeLog,$linha);
        }
       Catch(\Exception $e){

            echo "Nao foi possivel gravar o log, pasta sem permissao de escrita.";
        }
    }
    private function blackListIp($ip,$id,$horaDesbloqueio){
        try{
            file_put_contents($this->restricaoLog,$ip.",".$id.",".$horaDesbloqueio);
            file_put_contents($this->permanenteLog,$ip."\t - \t".$horaDesbloqueio);
        }Catch(\Exception $e){
            echo "Nao foi possivel gravar o log, pasta sem permissao de escrita.";
        }
    }
    private function strpos_array($haystack, $needles) {
        if ( is_array($needles) ) {
            foreach ($needles as $str) {
                if ( is_array($str) ) {
                    $pos = $this->strpos_array($haystack, $str);
                } else {
                    $pos = stripos($haystack, $str);
                }
                if ($pos !== FALSE) {
                    return $pos;
                }
            }
        } else {
            return stripos($haystack, $needles);
        }
        return false;
    }
    private function human_filesize($bytes, $decimals = 2) {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }
}