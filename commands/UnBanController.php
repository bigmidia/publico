<?php
/**
 * Created by Bigmidia
 * User: Eduardo Estevão
 * Date: 15/07/2016
 * Time: 11:46
 * base: https://www.sans.org/reading-room/whitepapers/logging/detecting-attacks-web-applications-log-files-2074
 * base: https://www.sans.org/reading-room/whitepapers/detection/identify-malicious-http-requests-34067
 * base: https://www.sans.org/reading-room/whitepapers/logging/evil-lens-web-logs-33950
 *
 */

namespace app\commands;


use app\models\CloudFlareApi;
use yii\console\Controller;

class UnBanController extends Controller
{
    private $restricaoLog="";
    private $runtimeLog="";
    public function actionIndex($log = '')
    {
        $this->runtimeLog=\Yii::getAlias('@runtime').DIRECTORY_SEPARATOR."heimdall.log";
        $this->restricaoLog=\Yii::getAlias('@runtime').DIRECTORY_SEPARATOR."heimdall_block.log";
        $newFile="";
        try {
            $handle = fopen($this->restricaoLog, "r");
            if ($handle) {
                while (($line = fgets($handle)) !== false) {

                    $dados = explode(",", $line);
                    if (count($dados) !== 3) {
                        continue;
                    }

                    if (isset($dados[2]) && isset($dados[1])) {

                        if ($dados[2] < time()) {
                            $cloud = new CloudFlareApi();
                            $cloud->unBlockIP($dados[1]);

                        } else {
                            $newFile .= $line;
                        }
                    }


                }
                fclose($handle);
                file_put_contents($this->restricaoLog, $newFile);
            }
        }Catch(\Exception $e){
            echo $e->getMessage();
        }

    }

}