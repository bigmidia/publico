Análise de Logs - API CloudFlare
============================

Estes scripts tem por objetivo procurar por conhecidas formas de ataques e banir os ips
via API no CloudFlare.

Formato do log a ser analisado
-------------------
NGINX
```php
log_format  wafS  '$remote_addr |-| $remote_user |-| $msec |-| "$request"  $status $body_bytes_sent "$http_user_agent"  "$http_x_forwarded_for"';
```


Forma de Uso
------------

~~~
./yii log-analyzer/index /caminho/para/o/log
~~~


Informações
------------

### ***ESTE SCRIPT NÃO SUBSTITUI UM WAF***

Este script tem por objetivo ajudar na segurança de aplicações web, mas sendo mais uma camada numa estratégia de defesa por profundidade
portanto o mesmo não tem por objetivo de substituir ferramentas de suma importância para a segurança da aplicação, o objetivo é somar e não substituir.



### Configurações

```php
$diferenca=120; // Esta variável no LogAnalyzerController define o tempo de log a ser analisado, no caso 120 define analisar apenas os últimos 2 minutos.
```

### CloudFlare

É necessário preencher os dados de email de autenticação e a chave da api para ser utilizado pelo script.
```php
private $auth_email="";
private $auth_key="";
```